"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Host = /** @class */ (function () {
    function Host(hostname, port) {
        this.hostname = hostname;
        this.port = port;
    }
    return Host;
}());
var DSN = /** @class */ (function () {
    function DSN(input) {
        var _this = this;
        this.options = {};
        this.hosts = [];
        this.protocol = input.protocol;
        this.dbname = input.dbname || input.db || input.database;
        this.username = input.username || input.user;
        this.password = input.password || input.pass;
        if (input.hosts) {
            input.hosts.forEach(function (host) {
                _this.hosts.push(new Host(host.hostname || host.host, host.port));
            });
        }
        else {
            this.hosts.push(new Host(input.hostname || input.host, input.port));
        }
        if (input.options) {
            Object.keys(input.options).forEach(function (key) { return _this.options[key] = input.options[key]; });
        }
    }
    DSN.prototype.build = function () {
        var _this = this;
        this._validateData();
        var credentials = this.username && this.password ?
            this.username + ":" + this.password + "@" :
            "";
        var hosts = this.hosts
            .map(function (host) { return "" + host.hostname + (host.port ? ":" + host.port : ""); })
            .join(",");
        var options = Object.keys(this.options)
            .map(function (key) { return encodeURIComponent(key) + "=" + encodeURIComponent(_this.options[key]); })
            .join("&");
        return this.protocol + "://" + credentials + hosts + "/" + this.dbname + (options ? "?" + options : "");
    };
    DSN.prototype._validateData = function () {
        if (!this.protocol) {
            throw new Error("The DSN is missing a protocol.");
        }
        if (this.hosts.some(function (host) { return !host.hostname; })) {
            throw new Error("The DSN is missing a host to connect to.");
        }
        if (!this.dbname) {
            throw new Error("The DSN is missing a database name.");
        }
        if (!!this.password && !this.username) {
            throw new Error("The DSN is missing a user name.");
        }
        if (!!this.username && !this.password) {
            throw new Error("The DSN is missing a password.");
        }
    };
    return DSN;
}());
exports.uDSN = function (input) { return new DSN(input).build(); };
