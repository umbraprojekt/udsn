const gulp = require("gulp");
const runSequence = require("run-sequence");

gulp.task("rebuild", () => runSequence("ts:dev", "test"));
