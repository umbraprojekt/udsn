**uDSN** is a simple DSN (Data Source Name) builder.

```
const uDSN = require("udsn").uDSN;

describe("uDSN", () => {
    it("should create a DSN", () => {
        const input = {
            protocol: "mongodb",
            hostname: "example.com",
            dbname: "myDatabase"
        };
        const dsn = uDSN(input);
        
        expect(dsn).to.equal("mongodb://example.com:27017/myDatabase");
    });
});
```

## Install

```
npm install --save udsn
```

## How to use

```
const uDSN = require("udsn").uDSN;
const input = {
    protocol: "mongodb",
    host: "localhost",
    dbname: "myDatabase"
};

const dsn = uDSN(input);
```

The `uDSN` function returns the built DSN as a string.

The input for the `uDSN` function is an object containing the following fields:

* `protocol` (required): the DSN protocol, e.g. `"mongodb"` or `"mysql"`
* `dbname` or `db` or `database` (required): the name of the database to connect to
* `hostname` or `host` (required/optional*): the host name of the database server
* `hosts` (required/optional*): an array of hosts to connect to. Each host contains the following fields:
    * `hostname` or `host` (required): database server host name
    * `port` (optional): database server port
* `port` (optional): port of the database server
* `options` (optional): an object containing additional options in the key-value format
* `username` or `user` (required/optional**): user name of the user connecting to the database
* `password` or `pass` (required/optional**): password for the database user

\* one of `host`/`hostname` or `hosts` is required. If `hosts` is present, it's required to contain at least one item.

\*\* `user`/`username` and `pass`/`password` must either be both present or both absent.

Example of valid input objects:

```
{
    protocol: "mongodb",
    hostname: "localhost",
    dbname: "myDatabase"
}
// mongodb://localhost/myDatabase
```

```
{
    protocol: "mongodb",
    port: 27018,
    host: "example.com",
    db: "myDatabase",
    user: "johndoe",
    pass: "Passw0rd"
}
// mongodb://johndoe:Passw0rd@example.com:27018/myDatabase
```

```
{
    protocol: "mongodb",
    database: "myDatabase",
    hosts: [
        {
            host: "localhost"
        },
        {
            host: "localhost"
            port: 27018
        }
    ]
}
// mongodb://localhost,localhost:27018/myDatabase
```

```
{
    protocol: "mysql",
    host: "localhost",
    db: "my_database",
    options: {
        useSSL: "true",
        verifyServerCertificate: "false"
    }
}
// mysql://localhost/my_database?useSSL=true&verifyServerCertificate=false
```

```
{
    protocol: "pgsql",
    host: "localhost",
    dbname: "my_database"
}
// pgsql://localhost/my_database
```
