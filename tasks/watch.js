const gulp = require("gulp");

gulp.task("watch", function() {
	gulp.watch(["src/**/*.ts"], ["rebuild"]);
	gulp.watch(["spec/**/*"], ["test"]);
});
