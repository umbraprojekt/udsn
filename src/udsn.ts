class Host {
	public hostname: string;
	public port: string;

	constructor(hostname: string, port: string) {
		this.hostname = hostname;
		this.port = port
	}
}

class DSN {
	private protocol: string;
	private dbname: string;
	private username: string;
	private password: string;
	private options: {[key: string]: string} = {};
	private hosts: Array<Host> = [];

	constructor(input: any) {
		this.protocol = input.protocol;
		this.dbname = input.dbname || input.db || input.database;
		this.username = input.username || input.user;
		this.password = input.password || input.pass;

		if (input.hosts) {
			input.hosts.forEach((host: any) => {
				this.hosts.push(new Host(
					host.hostname || host.host,
					host.port
				));
			});
		} else {
			this.hosts.push(new Host(
				input.hostname || input.host,
				input.port
			));
		}

		if (input.options) {
			Object.keys(input.options).forEach((key: string) => this.options[key] = input.options[key]);
		}
	}

	public build(): string {
		this._validateData();

		const credentials = this.username && this.password ?
			`${this.username}:${this.password}@` :
			"";
		const hosts = this.hosts
			.map(host => `${host.hostname}${host.port ? ":" + host.port : ""}`)
			.join(",");
		const options = Object.keys(this.options)
			.map(key => `${encodeURIComponent(key)}=${encodeURIComponent(this.options[key])}`)
			.join("&");
		return `${this.protocol}://${credentials}${hosts}/${this.dbname}${options ? "?" + options : ""}`;
	}

	private _validateData(): void {
		if (!this.protocol) {
			throw new Error("The DSN is missing a protocol.");
		}
		if (this.hosts.some(host => !host.hostname)) {
			throw new Error("The DSN is missing a host to connect to.");
		}
		if (!this.dbname) {
			throw new Error("The DSN is missing a database name.");
		}
		if (!!this.password && !this.username) {
			throw new Error("The DSN is missing a user name.");
		}
		if (!!this.username && !this.password) {
			throw new Error("The DSN is missing a password.");
		}
	}
}

export const uDSN: (input: any) => string = (input: any) => new DSN(input).build();
