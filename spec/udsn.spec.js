const expect = require("chai").expect;
const uDSN = require("../lib/udsn").uDSN;

describe("uDSN", () => {
	it("should correctly build a DSN from minimal set of data", () => {
		const input = {
			protocol: "mongodb",
			dbname: "test",
			host: "localhost"
		};
		expect(uDSN(input))
			.to.equal("mongodb://localhost/test");
	});

	it("should correctly build a DSN from full data set", () => {
		const input = {
			protocol: "mongodb",
			host: "example.com",
			dbname: "test",
			port: 27017,
			username: "user",
			password: "password"
		};
		expect(uDSN(input))
			.to.equal("mongodb://user:password@example.com:27017/test");
	});

	it("should correctly build DSN using alternative key names", () => {
		const input1 = {
			protocol: "mongodb",
			hostname: "example.com",
			db: "test",
			username: "user",
			password: "password"
		};
		const input2 = {
			protocol: "mongodb",
			hostname: "example.com",
			database: "test",
			user: "user",
			pass: "password"
		};
		expect(uDSN(input1))
			.to.equal("mongodb://user:password@example.com/test");
		expect(uDSN(input2))
			.to.equal("mongodb://user:password@example.com/test");
	});

	it("should correctly build DSN for multiple hosts", () => {
		const input = {
			protocol: "mongodb",
			hosts: [
				{
					host: "localhost"
				},
				{
					host: "localhost",
					port: 27018
				},
				{
					hostname: "example.com"
				},
				{
					hostname: "example.com",
					port: 27018
				}
			],
			dbname: "test"
		};
		expect(uDSN(input))
			.to.equal("mongodb://localhost,localhost:27018,example.com,example.com:27018/test");
	});

	it("should throw when no host is specified", () => {
		const input = {
			protocol: "mongodb",
			db: "test"
		};
		expect(() => uDSN(input))
			.to.throw("The DSN is missing a host to connect to.");
	});

	it("should throw when no protocol is specified", () => {
		const input = {
			db: "test",
			host: "localhost"
		};
		expect(() => uDSN(input))
			.to.throw("The DSN is missing a protocol.");
	});


	it("should throw when no database is specified", () => {
		const input = {
			protocol: "mongodb",
			host: "localhost"
		};
		expect(() => uDSN(input))
			.to.throw("The DSN is missing a database name.");
	});

	it("should throw when a password and no username is specified", () => {
		const input = {
			protocol: "mongodb",
			host: "localhost",
			db: "test",
			password: "password"
		};
		expect(() => uDSN(input))
			.to.throw("The DSN is missing a user name.");
	});

	it("should throw when a username and no password is specified", () => {
		const input = {
			protocol: "mongodb",
			host: "localhost",
			db: "test",
			username: "user"
		};
		expect(() => uDSN(input))
			.to.throw("The DSN is missing a password.");
	});

	it("should allow using options", () => {
		const input = {
			protocol: "mongodb",
			host: "localhost",
			db: "test",
			options: {
				foo: "bar",
				bat: "man",
				"a b": "c/d"
			}
		};
		expect(uDSN(input))
			.to.equal("mongodb://localhost/test?foo=bar&bat=man&a%20b=c%2Fd");
	});
});
